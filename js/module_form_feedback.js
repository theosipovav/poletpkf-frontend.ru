$(document).ready(function () {
    var itemInput = $('.form').find('.form__input');

    $.each(itemInput, function (indexInArray, valueOfElement) {
        if ($(valueOfElement).val()) {
            $(valueOfElement).parent('.form__group').addClass('from__value');
        } else {
            $(valueOfElement).parent('.form__group').removeClass('from__value');
        }

    });


    itemInput.focus(function () {
        $(this).parent('.form__group').addClass('form__focus');
    });

    $(itemInput).blur(function () {
        $(this).parent('.form__group').removeClass('form__focus');
    });

    $(itemInput).change(function (e) {
        if ($(this).val()) {
            $(this).parent('.form__group').addClass('from__value');
        } else {
            $(this).parent('.form__group').removeClass('from__value');
        }

    });

});