$(document).ready(function () {
    // Основное навигационное меню
    $("#NavbarMainMenu").lavalamp({
        duration: 1000,
        activeObj: ".current",
        setOnClick: true,
        delayOn: 0,
        delayOff: 1000,
        target_child: 'a'
    });
    $("#NavbarMainMenu .nav_item").click(function (e) {
        $("#NavbarMainMenu .current").removeClass("current");
        $(this).addClass("current");
    });

    // Форма поиска
    const overlay_magnifier = document.querySelector(".overlay_magnifier");
    const overlay_close = document.querySelector(".overlay_close");
    const search = document.querySelector(".search");
    const input = document.querySelector(".input");
    overlay_magnifier.addEventListener("click", () => {
        search.classList.toggle("active2");
        if (search.classList.contains("active2")) {
            setTimeout(() => {
                input.focus();
            }, 500);
        }
    });
    search.addEventListener("click", () => {
        if (search.classList.contains("active2")) {
            setTimeout(() => {
                input.focus();
            }, 500);
        }
    });
    overlay_close.addEventListener("click", e => {
        input.value = "";
        input.focus();
        search.classList.remove("searching");
    });
    document.body.addEventListener("click", e => {
        if (!search.contains(e.target) && input.value.length === 0) {
            search.classList.remove("active2");
            search.classList.remove("searching");
            input.value = "";
        }
    });
    input.addEventListener("keyup", e => {
        if (e.keyCode === 13) {
            input.blur();
        }
    });
    input.addEventListener("input", () => {
        if (input.value.length > 0) {
            search.classList.add("searching");
        } else {
            search.classList.remove("searching");
        }
    });
    input.value = "";
    input.blur();


    if ($(this).scrollTop() > 100) {
        $('.navbar').addClass("navbar_scroll");
    }
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.navbar').addClass("navbar_scroll");
        }
        else {
            $('.navbar').removeClass("navbar_scroll");
        }
    });
});
