$(document).ready(function () {
    $("#CarouselProduct .cartech__menu").lavalamp({
        easing: "easeOutBack",
        duration: 1000,
        activeObj: ".current",
        setOnClick: true
    });
    $("#CarouselProduct .cartech__menu button").click(function (e) {
        let nav_li = $(this).parent();
        $("#CarouselProduct .cartech__menu .active").removeClass("active");
        nav_li.addClass("active");
        $("#CarouselProduct .cartech__slides .active").removeClass("active");
        let slides = $("#CarouselProduct .carousel_products__slide");
        $(slides[nav_li.index() - 1]).addClass("active");
    });



    $("#CarTechBlog .cartech2__nav_menu").lavalamp({
        easing: "easeOutBack",
        duration: 1000,
        activeObj: ".active",
        setOnClick: true
    });

    $("#CarTechBlog .cartech2__nav_menu button").click(function (e) {
        let elementLiActive = $(this).parent();
        console.log(elementLiActive);

        let elementLiSlidesImg = $("#CarTechBlog .cartech2__slide_img");
        let elementLiSlidesText = $("#CarTechBlog .cartech2__slide_text");
        $("#CarTechBlog .cartech2__nav .active").removeClass("active");
        $("#CarTechBlog .cartech2__slides .active").removeClass("active");
        elementLiActive.addClass("active");
        $(elementLiSlidesImg[elementLiActive.index() - 1]).addClass("active");
        $(elementLiSlidesText[elementLiActive.index() - 1]).addClass("active");

    });

});

